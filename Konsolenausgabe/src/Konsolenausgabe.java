
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Meine Variablen
		
		String name = "Aufgabe";
		String s = "*************";
		double Zahl1 = 22.4234234;
		double Zahl2 = 111.2222;
		double Zahl3 = 4.0;
		double Zahl4 = 1000000.551;
		double Zahl5 = 97.34;
		
		//Aufgabe 1 = Spielen mit der Ausgabe
		
		System.out.print("->Das ist die \"Aufgabe\" 1<-\n\n");
		System.out.print("Jetzt kommt die "+ name +"  2\n\n");
		
		// Aufgabe 2 = Pfeil erstellen mit printf
		
		System.out.printf("%7.1s\n", s);
		System.out.printf("%8.3s\n", s);
		System.out.printf("%9.5s\n", s);
		System.out.printf("%10.7s\n", s);
		System.out.printf("%11.9s\n", s);
		System.out.printf("%12.11s\n", s);
		System.out.printf("%.13s\n", s);
		System.out.printf("%8.3s\n", s);
		System.out.printf("%8.3s\n", s);
		System.out.printf("%8.3s\n\n", s);
		
		System.out.print("Nun kommt die "+ name +" 3!\n\n");
		
		// Aufgabe 3 = Zahlen formatieren
		
		
		System.out.printf("%.2f\n", Zahl1);
		System.out.printf("%.2f\n", Zahl2);
		System.out.printf("%.2f\n", Zahl3);
		System.out.printf("%.2f\n", Zahl4);
		System.out.printf("%.2f\n", Zahl5);
				
		
	}

}
