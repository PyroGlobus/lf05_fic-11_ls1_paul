
public class KonsolenAusgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Aufgabe 1

String e = "**";
		
		System.out.printf("%7.2s\n", e);
		System.out.printf("%3.1s\t", e);
		System.out.printf("%2.1s\n", e);
		System.out.printf("%3.1s\t", e);
		System.out.printf("%2.1s\n", e);
		System.out.printf("%7.2s\n\n\n", e);
		
		//Aufgabe 2"
		
		String A = "= 1 * 2 * 3 * 4 * 5";
		
		//Zeile 1
		System.out.printf("%-5s", "0!");
		System.out.printf("%-18s", "=");
		System.out.printf("%5s\n", "= 1");
		//Zeile 2
		System.out.printf("%-5s", "1!");
		System.out.printf("%-18.3s", A);
		System.out.printf("%5s\n", "= 1");
		//Zeile 3
		System.out.printf("%-5s", "2!");
		System.out.printf("%-18.7s", A);
		System.out.printf("%5s\n", "= 2");
		//Zeile 4
		System.out.printf("%-5s", "3!");
		System.out.printf("%-18.11s", A);
		System.out.printf("%5s\n", "= 6");
		//Zeile 5
		System.out.printf("%-5s", "4!");
		System.out.printf("%-19.15s", A);
		System.out.printf("%5s\n", "= 24");
		//Zeile 6
		System.out.printf("%-5s", "5!");
		System.out.printf("%-20s", A);
		System.out.printf("%5s\n\n\n\n", "= 120");
		
		//Aufgabe 3
		
		String O = "-----------------------";
		
		System.out.printf("%8s", "Fahrenheit |");
		System.out.printf("%-10s\n", "   Celsius");
		System.out.printf("%s\n"	,	O);
		System.out.printf("%-10s", "-20");
		System.out.printf("%11s\n", "|  \t-28.89");
		System.out.printf("%-10s", "-10");
		System.out.printf("%11s\n", "|  \t-23.33");
		System.out.printf("%-10s", "+0");
		System.out.printf("%11s\n", "|  \t-17.78");
		System.out.printf("%-10s", "+20");
		System.out.printf("%11s\n", "|  \t -6.67");
		System.out.printf("%-10s", "+30");
		System.out.printf("%11s\n", "|  \t -1.11");
		
		
	}

}
