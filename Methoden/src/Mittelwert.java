import java.util.*;

public class Mittelwert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	      // (E) "Eingabe"
		Scanner myScanner = new Scanner(System.in);
	      // Werte f�r x und y festlegen:
	      // ===========================
	      double x, y, m;
	      
	      int Zahl = 43;
	      
	     System.out.println("Dieses Programm berechnet den Mittelwert zwei Zahlen!");
	     System.out.print("Bitte geben Sie die erste Zahl ein.");
	     
	     x = myScanner.nextDouble();
	     
	     System.out.print("Bitte geben Sie die zweite Zahl ein.");
	     
	     y = myScanner.nextDouble(); 
	     
	     
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      m = (x + y) / 2;
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);

	}

}
